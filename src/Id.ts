let lastId: number = 0;
//let objs: Object = new Object();

public function NewId(): number {
  return ++lastId;
}

/*
public function GetById(id: number): any {
  return objs[id];
}*/

public class IdObj {
  private id: number;

  constructor() {
    this.id = NewId();
    //objs[this.id] = this;
  }

  public getId(): number {
    return this.id;
  }

  public unreg(): void {
    //delete objs[this.id];
    this.id = null;
  }
}
