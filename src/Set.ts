import { IdObj } from "./Id";
import { YaArray } from "./Array";

public class YaSet<T extends IdObj> {
  private core: Object;
  private size: number;

  constructor() {
    this.clear();
  }

  public getSize(): number {
    return this.size;
  }

  public clear(): void {
    this.core = new Object();
    this.size = 0;
  }

  public remove(obj: T): boolean {
    if (this.has(obj)) {
      delete this.core[obj.getId()];
      this.size--;
      return true;
    } else {
      return false;
    }
  }

  public has(obj: T): boolean {
    return obj.getId() in this.core;
  }

  public add(obj: T): boolean {
    if (this.has(obj)) {
      return false;
    } else {
      this.core[obj.getId()] = obj;
      this.size++;
      return true;
    }
  }

  public toArray(): YaArray<T> {
    let ret: YaArray<T> = new YaArray<T>.emptyArray(this.size);
    let n: number = 0;
    for (var property in this.core) {
      if (this.core.hasOwnProperty(property)) {
        ret.set(n++, this.core[property]);
      }
    }
    return ret;
  }
}
