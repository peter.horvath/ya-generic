public class YaArray<T extends IdObj> {
  private core: any[];
  private size: number;

  public static newEmpty<T extends IdObj>(size: number): YaArray<T> {
    let ret: YaArray<T> = new YaArray<T>();
    ret.core = new Array(size);
    ret.size = size;
    return ret;
  }

  public get(idx: number): T {
    if ((idx < 0) || (idx >= size)) {
      throw new RangeError();
    }
    return this.core[idx];
  }

  public set(idx: number, t: T): void {
    if ((idx < 0) || (idx >= size)) {
      throw new RangeError();
    }
    this.core[idx] = t;
  }

  public getSize(): number {
    return this.size;
  }

  public setSize(s: number): void {
    if (s < 0) {
      return new RangeError();
    }
    this.core = new Array(s);
    this.size = s;
  }
}
